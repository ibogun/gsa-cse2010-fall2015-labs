
public class Editor {
	
	LinkedList<Character> content;  // this is LinkedList class you implemented
	// submission which use java.util.LinkedList will get 0 for this java class.

	public Editor(){
		content = new LinkedList<Character>();
	}
	
	public void insert(int location, String s){
		// insert characters from string into linked list content 
		// starting from 'location'
	}
	
	public void insert(int location, Character s){
		// insert Character 's' at 'location' in the linked list
	}
	
	public void insertInTheEnd(Character s){
		// insert char in the end
	}
	
	
	public void insertInTheEnd(String s){
		// insert characters from 's' in the end
	}
	
	
	public Character delete(int location){
		// delete character at location
	}
	
	public String delete(int from, int to){
		// delete characters given range
	}
	
	
	public String showText(){
		// return String which consists of all characters of the linked list
	}
	

}
