public class Graph {
	private int V; // number of vertices
	private int E; // number of edges
	private ArrayList<Integer>[] adj; // adjacency list representation 
	

	public Graph(int V){
		// constructor
	}


	public int V(){
		return V;
	}

	public int E(){
		return E;
	}


	// Perform Bread-First search and return a graph representing it. 
	public Graph bfs(int s) {
		
	}

	// Perform Depth-First search and return a graph representing it. 
	public Graph dfs(int s){

	}


	public void addEdge(int v, int w){
		// add edge between vertex v and vertex w
	}

	public ArrayList<Integer> adj(int v){
		return adj[v];
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		String NEWLINE = System.getProperty("line.separator");
		s.append(V + " vertices, " + E + " edges " + NEWLINE);
		for (int v = 0; v < V; v++) {
			s.append(v + ": ");
			for (int w : adj[v]) {
				s.append(w+" ");
			}
			s.append(NEWLINE);
		}
		return s.toString();
	}


	/**Override equals methods for graphs: compare number of edges, vertices and the
	 * adjacency lists correspondence. Nothing to implement here ( will be used for grading)
	 * 
	 */
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Graph)) {
			return false;
		}

		Graph that = (Graph) other;

		boolean isAdjSame=true;

		// iterate over adjacency list to check if they are the same
		try{
			for (int i = 0; i < Math.max(this.adj.length, that.adj.length); i++) {
				// sort so that order doesn't matter
				Collections.sort(this.adj[i]);
				Collections.sort(that.adj[i]);
				for (int j = 0; j < Math.max(this.adj[i].size(), that.adj[i].size()); j++) {
					if (this.adj[i].get(j)!=that.adj[i].get(j)){
						isAdjSame=false;
						// once at least one is found there is no need to continue
						break;
					}
				}
				if (!isAdjSame) break;
			}

		}catch(ArrayIndexOutOfBoundsException e){
			isAdjSame=false;
		}

		// if graphs are the same all should match
		return this.V==(that.V)&& this.E==(that.E)&&isAdjSame;
	}

}
