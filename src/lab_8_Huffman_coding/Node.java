
public class Node {

	private Node childLeft;
	private Node childRight;
	private Character key;
	private int frequences;

	public Node getChildLeft() {
		return childLeft;
	}

	public void setChildLeft(Node childLeft) {
		this.childLeft = childLeft;
	}

	public void setChildRight(Node childRight) {
		this.childRight = childRight;
	}

	public void setFrequency(int frequences) {
		this.frequences = frequences;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public Node getChildRight() {
		return childRight;
	}

	public int getFrequency() {
		return frequences;
	}

	public Character getKey() {
		return key;
	}

}
