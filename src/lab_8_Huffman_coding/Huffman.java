
public class Huffman {

	private Node root = null; // Tree representing Huffman code

	public Node getHuffmanTree(){
		return root;
	}

	/**
	 * Create Huffman tree given string	
	 * @param str - string to use to create Huffman tree
	 */
	public void createHuffmanTree(String str) {
	}

	/**
	 * Encode a given message using Huffman code and return a list of bits.
	 * @param message - to be encoded
	 * @return list of bits (code)
	 */
	public LinkedList<Boolean> encode(String message){
	}
	
	/**
	 * Decode list of bits into string
	 * @param code - string code
	 * @return Original string
	 */
	public String decode(LinkedList<Boolean> code){	
	}
	
	public String toString(){
		System.out.println("Tree:");
		return root.toString();
	}

}