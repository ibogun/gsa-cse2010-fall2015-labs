import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestArbitrage {
	CurrencyExchange currency;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		int n = 6;
		currency = new CurrencyExchange(n);
		
		currency.addExchange("A", "B", 0.4);
		currency.addExchange("B", "C", 0.5);
		currency.addExchange("B", "D", 1);
		currency.addExchange("A", "C", 0.2);
		currency.addExchange("D", "F", 1.1);
		currency.addExchange("C", "E", 0.3);
		currency.addExchange("E", "D", 0.4);
	}

	@Test
	public void testBellmanFord() {
		int n = 6;
		Graph graph = new Graph(n);

		graph.addWeightedEdge(0, 1, 0.4);
		graph.addWeightedEdge(1, 2, 0.5);
		graph.addWeightedEdge(1, 3, 1);
		graph.addWeightedEdge(0, 2, 0.2);
		graph.addWeightedEdge(3, 4, 1.1);
		graph.addWeightedEdge(2, 5, 0.3);
		graph.addWeightedEdge(5, 3, 0.4);

		// Note if there if answers[i][j] = 0 -> path does not exist, use 0 
		// only for convenience
		double[][] answers = new double[][] { 
			{ 0, 0.4, 0.2, 0.4, 0.44, 0.06 }, 
			{ 0, 0, 0.5, 1, 1.1, 0.15 },
				{ 0, 0, 0, 0.12, 0.132, 0.3 }, 
				{ 0, 0, 0, 0, 1.1, 0 }, };

		double epsilon = 0.00001;
		// there are no "negative cycles" in this graph
		for (int i = 0; i < 4; i++) {
			BellmanFord bf = new BellmanFord(graph, i);
			bf.findShortestPath();
			
			for (int j = i+1; j < n; j++) {
				if (answers[i][j]!=0) {
					assertEquals(answers[i][j], bf.getShortestPath(j), epsilon);
				}
			}
		}
	}
	
	@Test 
	public void testArbitrageOnlyOneCycle(){

		Arbitrage arbitrage = currency.findArbitrage();
		
		assertEquals(arbitrage, null);
		// create arbitrage
		currency.addExchange("F", "B", 0.95);	
		arbitrage = currency.findArbitrage();
		
		LinkedList<String> list=arbitrage.getLinks();
		
		String[] listCorrect ={"B", "D", "F", "B"};
		Iterator<String> it = list.iterator();
		
		int count = 0;
		while(it.hasNext()){
			String listElement = it.next();
			assertEquals(listElement, listCorrect[count]);
			count++;
		}
		
		assertEquals(arbitrage.isRealArbitrage(), true);
		
		
		assertEquals(arbitrage.getCost(), 1.045, 0.00001);
	}
	
	@Test 
	public void testArbitrageTwoCycles(){

		Arbitrage arbitrage = currency.findArbitrage();
		
		assertEquals(arbitrage, null);
		// create arbitrage
		currency.addExchange("F", "B", 0.95);	
		arbitrage = currency.findArbitrage();
		assertEquals(arbitrage.isRealArbitrage(), true);
		assertEquals(arbitrage.getCost(), 1.045, 0.00001);
		
		currency.addExchange("E", "A", 17.56);
		
		arbitrage = currency.findArbitrage();
		assertEquals(arbitrage.isRealArbitrage(), true);
		System.out.println(arbitrage);
		assertEquals(arbitrage.getCost()>1.045, true);
		
	}

}
