
import java.util.Stack;

public class SuperStack<Item extends Comparable<Item>> {
    private Stack<Item> main;  // main stack
    private Stack<Item> max;   // helper stack, use to implement functions using max element
    
    public SuperStack() {
    	main = new Stack<Item>();
		max = new Stack<Item>();
	}

    public final Item pop () {
    	// pop element from the SuperStack
    }
    public final Item peek () {
    	// peek (return) last element added, without removing the element
    	// from the SuperStack
    }

    public final void push (final Item i) {
    	// add element to the SuperStack
    }
    
    public final Stack<Item> getMainStack () {
    	return main;
    }

    public final Item max () {
    	// return, without removing, the largest element in the SuperStack
    }

    public final Item popMax () {
    	// return and remove the largest element in the SuperStack
    }

    public final boolean isEmpty () { 
    	return main.isEmpty();
    }

}

